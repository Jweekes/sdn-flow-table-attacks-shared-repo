#this is an example script to set up an OVS instance
ovs-vsctl del-br ovsbr-main
ovs-vsctl add-br ovsbr-main
ip addr add 172.16.1.1/16 dev ovsbr-main
ip link set dev ovsbr-main up
ovs-vsctl set bridge ovsbr-main protocols=OpenFlow13
ovs-vsctl set bridge ovsbr-main other-config:flow-eviction-threshold=5000
ovs-vsctl -- --id=@ft1 create Flow_Table name=table0 flow_limit=5000 overflow_policy=evict -- set Bridge ovsbr-main flow_tables:0=@ft1
ovs-vsctl set-controller ovsbr-main tcp:10.42.32.22:6633
ovs-vsctl add-port ovsbr-main enp0s20u1
ovs-vsctl add-port ovsbr-main enp0s20u2
ovs-vsctl add-port ovsbr-main enp0s26u1u2
ovs-vsctl add-port ovsbr-main enp0s26u1u1
ovs-vsctl show
