//in ofproto-provider.h 
	struct oftable{
	
		int evgflag;
		pthread_t evicThr;
	}
	
///ofproto/ofproto.c
//FUnction declarations- line 135
static void *evictionThreadHelper(struct ofproto *,struct oftable *)
 OVS_REQUIRES(ofproto_mutex);
static void *evictionThread(void *o);


	

static void
oftable_init(struct oftable *table)
{
...
	
	table->evgflag=0;
	table->evicThr= NULL;

}


void
ofproto_configure_table(struct ofproto *ofproto, int table_id,
                        const struct ofproto_table_settings *s)
{
    struct oftable *table;
	...


    if(s->name ){ //	sets for 1st table
	
		if(table->evgflag==0){
			table->evgflag=1;
			VLOG_INFO("Table ID: %d",table_id);
			//spawn thread for evictions
			pthread_t tid;
			pthread_create(&tid, NULL, evictionThread,(void*)ofproto);		//starts eviction thread- what happens when table is deleted?
			table->evicThr = tid;
			VLOG_INFO("Thread spawned: moving on");

		}
    }//end if (s->name)
    

}



static void
oftable_destroy(struct oftable *table)
{
    ovs_assert(classifier_is_empty(&table->cls));

    ovs_mutex_lock(&ofproto_mutex);
	pthread_kill(table->evicThr, SIGALRM);
    oftable_configure_eviction(table, 0, NULL, 0);
    ovs_mutex_unlock(&ofproto_mutex);

    hmap_destroy(&table->eviction_groups_by_id);
    heap_destroy(&table->eviction_groups_by_size);
    classifier_destroy(&table->cls);
    free(table->name);
}



/****************************	 EVICTION FUNCTIONS	 STARTS HERE - CAN BE PLACED ANYWHERE IN FILE************************************/

void *evictionThreadHelper(struct ofproto *ofproto, struct oftable *table)
     OVS_EXCLUDED(ofproto_mutex)
{
   		struct eviction_group *evg;
		int num_rules =0;
		int rule_exists=0;
		struct rule_collection rules;
        rule_collection_init(&rules);
		ovs_mutex_lock(&ofproto_mutex);
                 HEAP_FOR_EACH (evg, size_node, &table->eviction_groups_by_size) {
                        struct rule *rule;

                        HEAP_FOR_EACH (rule, evg_node, &evg->rules) {
                                //FIFO
                                if (rule){
                                        rule_exists=1;
                                        num_rules++;    //tester variable- to be deleted
                                }

                        /*      ovs_mutex_lock(&rule->mutex);
                                long long int created_ns = rule->created_ns;
                                ovs_mutex_unlock(&rule->mutex);
                        */

                                if(rule->state == RULE_INSERTED && rule->add_seqno >0){ //add_seq is set at end of rule addition- ensures rule add has completed           
                                        rule_collection_add(&rules, rule);

                                }

                        }

                }
                if (rule_exists==1){
//                        VLOG_INFO("evictionThread: Number of rules in rule collection: %d. Number of rules from prev section: %d",rule_collection_n(&rules),num_rules);
                      delete_flows__(&rules, OFPRR_EVICTION, NULL);
                        rule_collection_destroy(&rules); //not used in ofproto_rule_expire so may not be necessary

                }
  ovs_mutex_unlock(&ofproto_mutex);
}

void *evictionThread(void *o){

	
	struct timespec wait;
        struct ofproto *ofproto = (struct ofproto *) o;

	struct oftable *table = &ofproto->tables[0];
	
   	while (true){
		//long long int start = time_nsec();
		ovsrcu_quiesce_start();
		int rule_exists=0;
		wait.tv_sec = 0;
		wait.tv_nsec = 5000000;			//sleep for half a millisecond
		nanosleep(&wait, NULL);
		evictionThreadHelper(ofproto,table);
		ovsrcu_quiesce_end();	


	}
		
	table->evgflag=0;

	
}

/****************************	 END EVICTION FUNCTIONS	************************************/











/*************************** FOllowing portions deemed unnecessary but were used during testing so included in case anything goes wrong	***************/


static void
replace_rule_start(struct ofproto *ofproto, struct ofproto_flow_mod *ofm,
                   struct rule *old_rule, struct rule *new_rule)
{
		...
		
		     ovs_mutex_unlock(&old_rule->mutex);
            ovs_mutex_unlock(&new_rule->mutex);
        }
		if(old_rule->state== RULE_INSERTED){			//line inserted here
			/* Mark the old rule for removal in the next version. */
			cls_rule_make_invisible_in_version(&old_rule->cr, ofm->version);

			/* Remove the old rule from data structures. */
			ofproto_rule_remove__(ofproto, old_rule);
		}
    } else {
        table->n_flows++;
    }
	
	...
}

static void
delete_flows_start__(struct ofproto *ofproto, ovs_version_t version,
                     const struct rule_collection *rules)
    OVS_REQUIRES(ofproto_mutex)
{

    struct rule *rule;
    RULE_COLLECTION_FOR_EACH (rule, rules) {
        struct oftable *table = &ofproto->tables[rule->table_id];

	if (rule->state == RULE_INSERTED){							//line inserted here
	       table->n_flows--;
        	cls_rule_make_invisible_in_version(&rule->cr, version);

	        /* Remove rule from ofproto data structures. */
        	ofproto_rule_remove__(ofproto, rule);
	}


    }

}




static void
ofproto_rule_remove__(struct ofproto *ofproto, struct rule *rule)
    OVS_REQUIRES(ofproto_mutex)
{
struct ofputil_flow_mod fr;
minimatch_expand(&rule->cr.match, &fr.match);
struct in_addr src_inadd,dst_inadd;
src_inadd.s_addr=fr.match.flow.nw_src;
dst_inadd.s_addr=fr.match.flow.nw_dst;
char src_str[INET_ADDRSTRLEN];
char dst_str[INET_ADDRSTRLEN];
inet_ntop(AF_INET, &(src_inadd.s_addr), src_str, INET_ADDRSTRLEN);
inet_ntop(AF_INET, &(dst_inadd.s_addr), dst_str, INET_ADDRSTRLEN);

//VLOG_INFO("ofproto_rule_remove__: RULE: %s -> %s STATE = %d. Add_seq: %"PRIu64 " assert edited", src_str, dst_str, rule->state, rule->add_seqno);
//    ovs_assert(rule->state == RULE_INSERTED);
	if(rule->state == RULE_INSERTED){

    cookies_remove(ofproto, rule);

    eviction_group_remove_rule(rule);
    if (!ovs_list_is_empty(&rule->expirable)) {
        ovs_list_remove(&rule->expirable);
    }
    if (!ovs_list_is_empty(&rule->meter_list_node)) {
        ovs_list_remove(&rule->meter_list_node);
        ovs_list_init(&rule->meter_list_node);
    }
//VLOG_INFO("ofproto_rule_remove__: eviction_group_remove_rule done");

    /* Remove the rule from any groups, except from the group that is being
     * deleted, if any. */
    const struct rule_actions *actions = rule_get_actions(rule);

    if (actions->has_groups) {
        const struct ofpact_group *a;

        OFPACT_FOR_EACH_TYPE_FLATTENED(a, GROUP, actions->ofpacts,
                                        actions->ofpacts_len) {
            struct ofgroup *group;

            group = ofproto_group_lookup(ofproto, a->group_id, OVS_VERSION_MAX,
                                         false);
            ovs_assert(group);

            /* Leave the rule for the group that is being deleted, if any,
             * as we still need the list of rules for clean-up. */
            if (!group->being_deleted) {
                group_remove_rule(group, rule);
            }
        }
    }
//VLOG_INFO("ofproto_rule_remove__: actions remove done");

    rule->state = RULE_REMOVED;
//VLOG_INFO("ofproto_rule_remove__: Rule removed in ofproto_rule_remove__");
}
}