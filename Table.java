import java.util.*;
import java.io.*;

public class Table {

        public static int TABLE_SIZE = (500-1);
        //read a csv
        //build a hash table populated by the csv
        //limit the hashtable and record the times of packet arrivals
        //store each removal along with the time of the packet that disrupted it and its time



        public static void FIFO(String filename){
                 int testflowremoval125=0,testflowremoval128=0;

                try{
                        PrintWriter writer = new PrintWriter(filename+"_output_FIFO.txt", "UTF-8");

                Hashtable<String, Double> FT = new Hashtable<String,Double>();
                try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
                    String line;
                    Double prev=0.0;
                    int count = 0;
                //writer.println("Time of Eviction, Time since last eviction, Rule to be evicted, Age of rule");

                    while ((line = br.readLine()) != null) {

                       String temp[] = line.split(",");
                       //System.out.println(line);
                       String srcdst = temp[1]+"-"+temp[2];
                       if (!(FT.containsKey(srcdst))){
                           if (FT.size()>TABLE_SIZE){
                                   count++;
                                   double oldest = Double.MAX_VALUE;
                                   String oldestkey = "";
                                   Set<String> keys = FT.keySet();
                                        for(String key: keys){
                                                if (FT.get(key)<=oldest){ //(adding the "=" makes a huge difference. Not sure if accurate)
                                                        oldestkey = key;
                                                        oldest = FT.get(key);
                                                }
                                        //IS THIS CORRECT??? SEEMS INCORRECT. SEEMS LIKE IT GETS THE YOUNGEST
                                        }
                                        //System.out.println(count);
                               //if (count==1)
                                   //writer.println(temp[0]+","+String.format("%.9f",oldest)+","+oldestkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-oldest)));
                               //else
                                   //writer.println(temp[0]+","+String.format("%.9f",(Double.parseDouble(temp[0])-prev))+","+oldestkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-oldest)));
                                        // this prints out: the time the rule is to be evicted, the time between evictions so that the simulation can sleep,
                                        //the rule being evicted by src-dst, and the eviction criteria: in this case, duration of existence in the switch so that
                                        //it can be compared with the test rule. Duration calculated by time of deletion-time of entry
                                   FT.remove(oldestkey);
                                   prev = Double.parseDouble(temp[0]);
                                   if(oldestkey.equals("172.16.3.125-172.16.3.128"))
                                        testflowremoval125++;
                                if(oldestkey.equals("172.16.3.128-172.16.3.125"))
                                        testflowremoval128++;

                           }
                           FT.put(srcdst, Double.parseDouble(temp[0]));

                       }

                    }
                }catch (IOException e){
                        System.out.println("Problem");
                }
                writer.close();
                }catch (IOException e){

                }
        System.out.println("FIFO completed on "+filename);
        System.out.println("Test flow 125-128 removed "+ testflowremoval125 + " times. Freqency of "+ (int)(testflowremoval125/60) + " per second over 60 seconds");
        System.out.println("Test flow 128-125 removed "+ testflowremoval128 + " times. Freqency of "+ (int)(testflowremoval128/60) + " per second over 60 seconds");
                System.out.println("\n");

        }


        public static void LFU(String filename){
                 int testflowremoval125=0,testflowremoval128=0;
                try{
                        PrintWriter writer = new PrintWriter(filename+"_output_LFU.txt", "UTF-8");

                Hashtable<String, Double> FT_PacArrTime = new Hashtable<String,Double>();
                Hashtable<String, Integer> FT_NumPackets = new Hashtable<String,Integer>();
                try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
                    String line;
                    Double prev=0.0;    //previous time of eviction
                    int count = 0;
                //writer.println("Time of Eviction, Time since last eviction, Rule to be evicted, Age of rule, Num Packets in rule, Freq of Rule");
                    while ((line = br.readLine()) != null) {

                       String temp[] = line.split(",");
                       String srcdst = temp[1]+"-"+temp[2];
                       if (!(FT_PacArrTime.containsKey(srcdst))){
                           if (FT_PacArrTime.size()>TABLE_SIZE){
                                   double currtime = Double.parseDouble(temp[0]);



                                   count++;
                                   double lowestfreq = Double.MAX_VALUE;
                                   String lowestfreqkey = "";


                                   Set<String> keys = FT_PacArrTime.keySet();
                                        for(String key: keys){
                                                double duration = currtime - FT_PacArrTime.get(key);
                                                if (duration!=0){       //this assumes that there will not be 50 packets with the same timestamp
                                                        double freq = FT_NumPackets.get(key) / duration;
                                                        if (freq<lowestfreq){
                                                                lowestfreqkey = key;
                                                                lowestfreq = freq;
                                                        }
                                                }
                                        }

                                        //if (count==1)
                                                   //writer.println(temp[0]+","+String.format("%.9f",(currtime - FT_PacArrTime.get(lowestfreqkey)))+","+lowestfreqkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-FT_PacArrTime.get(lowestfreqkey)))+","+FT_NumPackets.get(lowestfreqkey)+","+lowestfreq);
                                       //else
                                           //writer.println(temp[0]+","+String.format("%.9f",(Double.parseDouble(temp[0])-prev))+","+lowestfreqkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-FT_PacArrTime.get(lowestfreqkey)))+","+FT_NumPackets.get(lowestfreqkey)+","+lowestfreq);
                                                        // this prints out: the time the rule is to be evicted, the time between evictions so that the simulation can sleep,
                                        //the rule being evicted by src-dst, and the eviction criteria: in this case, duration of existence in the switch so that
                                        //it can be compared with the test rule. Duration calculated by time of deletion-time of entry
                              if((lowestfreq == 1000000000.0) || (lowestfreqkey == "") ) //this shows all 50 packets arrived att he same time and flow table could not find one to remove. duration = Infinity for all
                                  System.out.println("No flow found to remove. Table now overfilled");

                                FT_PacArrTime.remove(lowestfreqkey);
                                FT_NumPackets.remove(lowestfreqkey);
                                prev = Double.parseDouble(temp[0]);
                                if(lowestfreqkey.equals("172.16.3.125-172.16.3.128"))
                                        testflowremoval125++;
                                if(lowestfreqkey.equals("172.16.3.128-172.16.3.125"))
                                        testflowremoval128++;

                           }
                           FT_PacArrTime.put(srcdst, Double.parseDouble(temp[0]));
                           FT_NumPackets.put(srcdst, 1);

                       }
                       else{//if it is already in the flow table, increase the count
                         //  if (srcdst=="172.16.3.125-172.16.3.128")
                                  // System.out.println("found additional packet for iperf. Packet num: "+(FT_NumPackets.get(srcdst)));
                           FT_NumPackets.put(srcdst, (FT_NumPackets.get(srcdst)+1));
                       }

                    }
                }catch (IOException e){

                }
                writer.close();
                }catch (IOException e){

                }
        System.out.println("LFU completed on "+filename);
        System.out.println("Test flow 125-128 removed "+ testflowremoval125 + " times. Freqency of "+ (int)(testflowremoval125/60) + " per second over 60 seconds");
        System.out.println("Test flow 128-125 removed "+ testflowremoval128 + " times. Freqency of "+ (int)(testflowremoval128/60) + " per second over 60 seconds");
                System.out.println("\n");

        }


        public static void LRU(String filename){
                 int testflowremoval125=0,testflowremoval128=0;

                try{
                        PrintWriter writer = new PrintWriter(filename+"_output_LRU.txt", "UTF-8");

                Hashtable<String, Double> FT = new Hashtable<String,Double>();
                try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
                    String line;
                    Double prev=0.0;
                    int count = 0;
                //writer.println("Time of Eviction, Time since last eviction, Rule to be evicted, Age of rule");

                    while ((line = br.readLine()) != null) {

                       String temp[] = line.split(",");
                       String srcdst = temp[1]+"-"+temp[2];
                       if (!(FT.containsKey(srcdst))){
                           if (FT.size()>TABLE_SIZE){
                                   count++;
                                   double oldest = Double.MAX_VALUE;
                                   String oldestkey = "";
                                   Set<String> keys = FT.keySet();
                                        for(String key: keys){
                                                if (FT.get(key)<oldest){
                                                        oldestkey = key;
                                                        oldest = FT.get(key);
                                                }

                                        }

                               //if (count==1)
                                   //writer.println(temp[0]+","+String.format("%.9f",oldest)+","+oldestkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-oldest)));
                               //else
                                   //writer.println(temp[0]+","+String.format("%.9f",(Double.parseDouble(temp[0])-prev))+","+oldestkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-oldest)));
                                        // this prints out: the time the rule is to be evicted, the time between evictions so that the simulation can sleep,
                                        //the rule being evicted by src-dst, and the eviction criteria: in this case, duration of existence in the switch so that
                                        //it can be compared with the test rule. Duration calculated by time of deletion-time of entry
                                   FT.remove(oldestkey);
                                   prev = Double.parseDouble(temp[0]);
                                   if(oldestkey.equals("172.16.3.125-172.16.3.128"))
                                        testflowremoval125++;
                                if(oldestkey.equals("172.16.3.128-172.16.3.125"))
                                        testflowremoval128++;

                           }
                           FT.put(srcdst, Double.parseDouble(temp[0]));

                       } else{//if it is already in the flow table, put time of current packet
                                 //  if (srcdst=="172.16.3.125-172.16.3.128")
                                  // System.out.println("found additional packet for iperf. Packet num: "+(FT_NumPackets.get(srcdst)));
                           FT.put(srcdst, Double.parseDouble(temp[0]));
                       }

                    }
                }catch (IOException e){

                }
                writer.close();
                }catch (IOException e){

                }
        System.out.println("LRU completed on "+filename);
        System.out.println("Test flow 125-128 removed "+ testflowremoval125 + " times. Freqency of "+ (int)(testflowremoval125/60) + " per second over 60 seconds");
        System.out.println("Test flow 128-125 removed "+ testflowremoval128 + " times. Freqency of "+ (int)(testflowremoval128/60) + " per second over 60 seconds");
                System.out.println("\n");

        }


        public static void RandRep(String filename){
                 int testflowremoval125=0,testflowremoval128=0;
                try{
                        PrintWriter writer = new PrintWriter(filename+"_output_RandRep.txt", "UTF-8");

                Hashtable<String, Double> FT = new Hashtable<String,Double>();
                try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
                    String line;
                    Double prev=0.0;
                    int count = 0;
                //writer.println("Time of Eviction, Time since last eviction, Rule to be evicted, Age of rule");

                    while ((line = br.readLine()) != null) {

                       String temp[] = line.split(",");
                       String srcdst = temp[1]+"-"+temp[2];

                       if (!(FT.containsKey(srcdst))){
                           if (FT.size()>TABLE_SIZE){
                                   Random rand = new Random();
                                   int  n = rand.nextInt(TABLE_SIZE) + 1;
                                   int a=0;
                                   count++;
                                   double randkeyvalue = 1000000000.0;
                                   String randkey = "";
                                   Set<String> keys = FT.keySet();
                                        for(String key: keys){
                                                if (a==n){
                                                        randkey = key;
                                                        randkeyvalue = FT.get(key);
                                                        break;
                                                }
                                                a++;
                                        }

                               //if (count==1)
                                   //writer.println(temp[0]+","+String.format("%.9f",randkeyvalue)+","+randkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-randkeyvalue)));
                               //else
                                   //writer.println(temp[0]+","+String.format("%.9f",(Double.parseDouble(temp[0])-prev))+","+randkey+","+String.format("%.9f",(Double.parseDouble(temp[0])-randkeyvalue)));
                                        // this prints out: the time the rule is to be evicted, the time between evictions so that the simulation can sleep,
                                        //the rule being evicted by src-dst, and the eviction criteria: in this case, duration of existence in the switch so that
                                        //it can be compared with the test rule. Duration calculated by time of deletion-time of entry
                                   FT.remove(randkey);
                                   prev = Double.parseDouble(temp[0]);
                                   if(randkey.equals("172.16.3.125-172.16.3.128"))
                                        testflowremoval125++;
                                if(randkey.equals("172.16.3.128-172.16.3.125"))
                                        testflowremoval128++;
                           }
                           FT.put(srcdst, Double.parseDouble(temp[0]));

                       }

                    }
                }catch (IOException e){

                }
                writer.close();
                }catch (IOException e){

                }
        System.out.println("Random Replacement completed on "+filename);
        System.out.println("Test flow 125-128 removed "+ testflowremoval125 + " times. Freqency of "+ (int)(testflowremoval125/60) + " per second over 60 seconds");
        System.out.println("Test flow 128-125 removed "+ testflowremoval128 + " times. Freqency of "+ (int)(testflowremoval128/60) + " per second over 60 seconds");
        System.out.println("\n");
        }


        public static void main(String [] args){

                String filename = args[0];
/*              String algorithm = args[0];


                switch(algorithm){
                        case ("FIFO"):
                                FIFO(filename);
                        case("LFU"):
                                LFU(filename);
                        case("LRU"):
                                LRU(filename);
                        case("Random"):
                                RandRep(filename);
                        default:
                                System.out.println("Invalid algorithm");

                }
*/
                FIFO(filename);
                LFU(filename);
                LRU(filename);
                RandRep(filename);
                //FIFO("C:/Users/Jonathan/Documents/PHD/Eviction/Simulation/combined_FIFO_55_caida_100_iperf.csv");
                //LFU("C:/Users/Jonathan/Documents/PHD/Eviction/Simulation/combined_FIFO_55_caida_100_iperf.csv");
                //LRU("C:/Users/Jonathan/Documents/PHD/Eviction/Simulation/combined_FIFO_55_caida_100_iperf.csv");
                //RandRep("C:/Users/Jonathan/Documents/PHD/Eviction/Simulation/combined_FIFO_55_caida_100_iperf.csv");

        }


}

