# this scrpit compiles OpenVSwitch from source on a RHEL system
rpm -e openvswitch
rm -rf /root/rpmbuild/SOURCES/open*
rm -rf /root/rpmbuild/BUILD/open*
rm -f /root/rpmbuild/RPMS/x86_64/openvswitch-2.5.0-1.x86_64.rpm
rm -f openvswitch-2.5.0.tar.gz
rm -f  /usr/local/etc/openvswitch/conf.db
tar -zcvf openvswitch-2.5.0.tar.gz openvswitch-2.5.0/
cp openvswitch-2.5.0.tar.gz /root/rpmbuild/SOURCES
cd openvswitch-2.5.0
./boot.sh
./configure
make dist
rpmbuild -bb --without check rhel/openvswitch.spec
#rpmbuild -bb --without check rhel/openvswitch.spec # for no test
rpm -ivh /root/rpmbuild/RPMS/x86_64/openvswitch-2.5.0-1.x86_64.rpm
mkdir -p /usr/local/etc/openvswitch
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
systemctl start openvswitch
cd ..

#source ovs_setup.sh